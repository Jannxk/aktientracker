package de.jannik.aktientracker.Gui;

import de.jannik.aktientracker.Processing.AktienGraph;
import de.jannik.aktientracker.methods.LadeDaten;

import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.*;

public class GUI extends JFrame {


    private static HashMap<Integer, ArrayList<Float>> lastMap = new HashMap<>();
    float[] kaufFloatWerte = new float[3];
    private Timer timer = new Timer();
    private Map<String, String> isins = LadeDaten.ladeIsin();

    public void createGUI() {
        this.setTitle("Aktientracker");
        this.setSize(800, 720);
        this.setVisible(true);
        this.setLayout(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        int anzahl = 3;
        JComboBox[] eingabe = new JComboBox[anzahl];
        JTextField[] kaufWert = new JTextField[anzahl];
        JTextField[] ausgabe = new JTextField[anzahl];
        JLabel kaufWertInfo = new JLabel("Kaufkurs");

        int breite = 180;
        int höhe = 20;
        //Definition von Textfelder und Knöpfen
        for (int i = 0; i < anzahl; i++) {
            eingabe[i] = new JComboBox();
            int finalI = i;
            //Trägt die Unternehmensnamen in die ComboBox ein
            isins.forEach((k, v) -> eingabe[finalI].addItem(k));
            ausgabe[i] = new JTextField();
            kaufWert[i] = new JTextField();

            kaufWert[i].setEditable(true);
            eingabe[i].setEditable(false);
            ausgabe[i].setEditable(false);

            eingabe[i].setBounds(100 + breite * i, 100, breite, höhe);
            ausgabe[i].setBounds(100 + breite * i, 30, breite, höhe);
            kaufWert[i].setBounds(100 + breite * i, 125, breite, höhe);

            eingabe[i].setVisible(true);
            ausgabe[i].setVisible(true);
            kaufWert[i].setVisible(true);


            this.add(eingabe[i]);
            this.add(ausgabe[i]);
            this.add(kaufWert[i]);

        }

        kaufWertInfo.setBounds(30,125,100,höhe);
        kaufWertInfo.setVisible(true);
        this.add(kaufWertInfo);
        //Lädt die Daten asynchron von TradeGate und speichert diese in einer HashMap; Abfrage und Speichern erfolt alle 10 sekunden.
        JButton ladeDaten = new JButton("Lade Daten");
        ladeDaten.setBounds(100, 200, breite, 40);
        ladeDaten.setVisible(true);
        this.add(ladeDaten);

        //Fügt die gesamte Knopflogik hinzu
        ladeDaten.addActionListener(e -> {
            //Wiederholt alle 10 Sekunden den Code. In diesem Fall: Lädt die aktuellen Daten und berechnet den Gewinn / Verlust
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    //Schleife irteriert über alle Eingabefelder
                    for (int i = 0; i < anzahl; i++) {
                        if (!eingabe[i].getSelectedItem().toString().isEmpty()) {
                            String aktienWert = LadeDaten.ladeDaten(isins.get(eingabe[i].getSelectedItem().toString())).last;
                            aktienWert = aktienWert.replace(",", ".");
                            float value = Float.parseFloat(aktienWert);
                            //Speichert die Werte in der Map<int,ArrayList>
                            if (lastMap.get(i) == null) {
                                lastMap.put(i, new ArrayList<>());
                                lastMap.get(i).add(value);
                            } else {
                                lastMap.get(i).add(value);
                            }
                            Color color = Color.BLACK;
                            if (!kaufWert[i].getText().equals("")) {
                                //Abfangen von falschen Eingabewerten und den Nutzer daraufhinweisen, mittels Popupfenster
                                kaufWert[i].setText(kaufWert[i].getText().replaceAll(",", "."));
                                //Überprüfung, sodass nur Zahlen möglich sind! Sonst Fehlermeldung
                                if (kaufWert[i].getText().matches("\\d+.?\\d*")) {
                                    kaufFloatWerte[i] = Float.parseFloat(kaufWert[i].getText());
                                    if (value > Float.parseFloat(kaufWert[i].getText()))
                                        color = Color.GREEN;
                                    else
                                        color = Color.RED;
                                } else {
                                    JOptionPane.showMessageDialog(null, "Falsche Eingabewerte!");
                                }
                            }


                            ausgabe[i].setForeground(color);
                            float erg = value - kaufFloatWerte[i];
                            ausgabe[i].setText(value + "€" + "\t" + erg);

                            AktienGraph.setKaufWert(kaufFloatWerte);

                        }
                    }
                }
            }, 0, 1000 * 10);

        });

        //Knopf definition
        JButton zeichneGraph = new JButton("Zeichne Graph");
        zeichneGraph.setBounds(100, 300, breite, 40);
        zeichneGraph.setVisible(true);
        this.add(zeichneGraph);
        //Knopf Logik
        zeichneGraph.addActionListener(e -> {
            //Übergabe der Map an Processing
            if (AktienGraph.getMap().isEmpty()) {
                AktienGraph.setMap(lastMap);
            }
            //Startet das Processingfenster
            AktienGraph.main("de.jannik.aktientracker.Processing.AktienGraph");
        });

        this.setVisible(true);

    }
}

package de.jannik.aktientracker.methods;

import com.google.gson.Gson;
import de.jannik.aktientracker.Objects.Aktie;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class LadeDaten {

    /**
     * Lädt die Daten von TradeGate
     *
     * @param isin - Nummer mit welcher die Daten abgerufen werden
     * @return Aktie
     */
    public static Aktie ladeDaten(String isin) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder(URI.create("https://www.tradegate.de/refresh.php?isin=" + isin)).header("accept", "application/json").build();
        CompletableFuture<HttpResponse<String>> responseCompletableFuture = client.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString());
        Gson gson = new Gson();
        try {
            HttpResponse<String> response = responseCompletableFuture.get();
            return gson.fromJson(response.body(), Aktie.class);
        } catch (ExecutionException | InterruptedException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * Lädt die Isin aus einer Datei
     *
     * @return Map<Name, Isin>
     */
    public static Map<String, String> ladeIsin() {
        Map<String, String> map = new HashMap<>();
        File file = new File("src/de/jannik/aktientracker/utils/ISIN.txt");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while (reader.ready()) {
                //Entfernt alle Leerzeichen
                String line = reader.readLine().replaceAll(" ", "");
                //Teilt nach Name, ISIN
                String[] split = line.split(",");
                map.put(split[0], split[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return map;
    }
}

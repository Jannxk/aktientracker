package de.jannik.aktientracker.Processing;

import processing.core.PApplet;

import java.util.ArrayList;
import java.util.HashMap;

public class AktienGraph extends PApplet {

    private static HashMap<Integer, ArrayList<Float>> map = new HashMap<>();
    private static float[] kaufWert = new float[3];
    private final int top = 360;

    public static HashMap<Integer, ArrayList<Float>> getMap() {
        return map;
    }

    public static void setMap(HashMap<Integer, ArrayList<Float>> map) {
        AktienGraph.map = map;
    }

    public static void setKaufWert(float[] kaufWert) {
        AktienGraph.kaufWert = kaufWert;
    }

    public static void main(String[] args) {
        PApplet.main("de.jannik.aktientracker.Processing.AktienGraph");
    }


    //Parameter welche einmal gesetzt werden müssen
    @Override
    public void setup() {
        super.setup();
        stroke(255);
        strokeWeight(0f);
        background(0);
    }

    // Generelle Einstellungen zum Fenste
    @Override
    public void settings() {
        super.settings();
        this.setSize(800, 520);
    }

    @Override
    public void draw() {
        //Achsen
        line(60, 40, 60, 400);
        line(60, 400, 700, 400);

        //Y-Achse
        for (int i = 0; i < 370; i = i + 10) {
            line(50, 40 + i, 70, 40 + i);
            text(top - i, 30, 40 + i);

        }
        //x Achse
        int x = 0;
        for (int i = 0; i < 640; i = i + 10) {
            line(60 + i, 390, 60 + i, 410);
            text(x++, 60 + i, 430);

        }
        //Zeichnet den Graphen anhand einer Werte ArrayList
        int[] x2 = new int[map.size()];
        for (int i = 0; i < map.size(); i++) {
            //Löscht Einträge, sodass es auf der Skala bleibt
            if (map.get(i).size() > 63) {
                map.get(i).remove(0);
            }
            for (int j = 0; j < map.get(i).size() - 1; j++) {
                float startX = x2[i] + 10 * j + 60;
                float startY = ((top + 40) - map.get(i).get(j));
                float endeX = x2[i] + 10 * j + 10 + 60;
                float endeY = ((top + 40) - map.get(i).get(j + 1));
                line(startX, startY, endeX, endeY);

            }
        }
        //Zeichnet eine Linie, für wie viel etwas gekauft wurde
        for (float v : kaufWert) {
            line(60, (top + 40) - v, 700, (top + 40) - v);
        }
    }
}

package de.jannik.aktientracker.Objects;

public class Aktie {

    public String bid;
    public String ask;
    public int bidsize;
    public int asksize;
    public String delta;
    public int stueck;
    public int umsatz;
    public double avg;
    public String last;
    public String high;
    public String low;

    @Override
    public String toString() {
        return "Aktie{" +
                "bid=" + bid +
                ", ask=" + ask +
                ", bidsize=" + bidsize +
                ", asksize=" + asksize +
                ", delta=" + delta +
                ", stueck=" + stueck +
                ", umsatz=" + umsatz +
                ", avg=" + avg +
                ", last=" + last +
                ", high=" + high +
                ", low=" + low +
                '}';
    }
}

